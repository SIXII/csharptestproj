﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.IO;

namespace Signature
{
    class Producer
    {
        protected volatile bool _isstoped = false;
        protected AutoResetEvent _waiting = new AutoResetEvent(false);
        protected Queue<byte[]> _queue = new Queue<byte[]>();
        public Thread p_thread;
        private readonly string _path;
        private readonly object _locker = new object();
        private readonly long _couples;
        private long _slength;
        private long _offset;
        protected int _SHA256block = 32768;
        protected volatile bool _islastblock = false;
        protected volatile bool _blockend = false;
        private FileStream _target;
        private readonly long _filelength;


        public Producer(string path, long slength, long filelength, long initoffset, int couples)
        {
            _path = path;
            _slength = slength;
            _offset = initoffset;
            _couples = couples * slength;
            if (_SHA256block > slength)
                _SHA256block = (int)slength;
            _filelength = filelength;
            p_thread = new Thread(ReadData);
            p_thread.Name = "Prod";
            p_thread.Start();
            
        }
        
        protected void ReadData()
        {
            using (_target = new FileStream(_path, FileMode.Open, FileAccess.Read, FileShare.Read, _SHA256block))
            {
                _target.Position = _offset;
                
                while (!_islastblock) {
                    
                    ReadBlock();

                    if ((_target.Position + _couples) >= _target.Length) {
                        _islastblock = true;
                        _isstoped = true;
                    }
                    else {
                        _blockend = false;
                        _target.Position += _couples;
                    }
                                   
                }
                
            }
        }


        private void ReadBlock() {

            int dataframe = _SHA256block;
            long bytesread = 0;
           // bool blockend = false;
            
            if ((_target.Position + _slength) > _filelength) 
                _slength = _filelength - _target.Position;

            while (!_blockend) {

                try {
                    byte[] p_datachunk = new byte[_SHA256block];

                    if ((bytesread + _SHA256block) >= _slength) {
                        dataframe = (int)(_slength - bytesread);
                        _blockend = true;
                    }

                    _target.Position += _target.Read(p_datachunk, 0, dataframe);
                    bytesread += dataframe;

                    WriteQueue(p_datachunk);
                }

                catch (OutOfMemoryException) {
                    _waiting.WaitOne();
                }
               
            }

            //_blockend = true;
            _waiting.WaitOne();
        }


        private void WriteQueue(byte[] datachunk) {
            lock (_locker) {
                _queue.Enqueue(datachunk);
                Monitor.Pulse(_locker);
            }
        }


        protected byte[] ReadQueue() {
            lock (_locker) {
                while (_queue.Count == 0) {
                    Monitor.Wait(_locker);
                }

                return _queue.Dequeue();
            }
        }


    }
}
