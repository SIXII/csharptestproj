﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.IO;
using System.Security.Cryptography;
using System.Diagnostics;

namespace Signature
{
    
     class Board
    {
        private static int _cores = Environment.ProcessorCount; //Откроем число потоков, равное числу ядер,
        private static long _sLength;                               //уменьшенному на 1. Один поток оставим для main
        private static string PATH;
        private static long _fLength;
        public static AutoResetEvent Signal = new AutoResetEvent(false);
        public static Synchronizer Queue;
        private static Stopwatch Timer = new Stopwatch();

        static void Main()
        {
            Init();
            if (_fLength / _sLength + ((_fLength % _sLength > 0) ? 1 : 0) < _cores)
                _cores = Convert.ToInt32(_fLength / _sLength);

            Queue = new Synchronizer(_sLength, _fLength, _cores / 2);
            Consumer[] arr = new Consumer[_cores];
            
            for(int i = 0; i < _cores/2; i++)
            {
                arr[i] = new Consumer(i + 1, PATH, _sLength, _fLength, i * _sLength, (_cores / 2) - 1);
            }
            Timer.Start();
            Signal.WaitOne();
            Timer.Stop();
            Console.WriteLine("Done in {0}",Timer.ElapsedMilliseconds);
            Console.ReadLine();


        }

        private static void Init() //Иницализация в отдельном методе исключительно для удобства
        {
            Console.WindowWidth += 20;
            try
            {
                if (_cores < 1) //И никаких фокусов с моноядерными машинами
                         throw new ArgumentException("More then one core is required for the functioning");
            }
            catch (Exception err)
            { ExceptionsManagement(err, true); }
                Console.Write("Signature length in bytes: ");

            
            try { _sLength = Int64.Parse(Console.ReadLine()); }
            catch (Exception err)
            { ExceptionsManagement(err, false); }

            Console.Write("FULL file path: ");
            try {
                PATH = Console.ReadLine();
                FileInfo target = new FileInfo(PATH);
                if (!target.Exists || target.Length == 0)
                    throw new ArgumentException("File does not exist or zero-length");
                _fLength = target.Length;
                if (_sLength > _fLength || _sLength <= 0 )
                    throw new ArgumentException("Block is too long or zero-length or negative value");
                target = null;
                
            }
            catch (Exception err)
            { ExceptionsManagement(err, false); }

           
        }

        private static void ExceptionsManagement(Exception Error, bool Exit) //Управление исключениями тоже вынесем в отдельный метод
        {
            Console.WriteLine("Message: {0}", Error.Message);
            Console.WriteLine("Stack Trace: {0}", Error.StackTrace);
            Console.ReadLine();
            if (!Exit) { Console.Clear(); Init(); }
            else Environment.Exit(1);
            
        }

        
    }
}
