﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Security.Cryptography;

namespace Signature
{
    class Consumer : Producer
    {
        private readonly int _id;
        private SHA256Managed _hash = new SHA256Managed();
        private Thread c_thread;
        
        public Consumer (int id, string path, long slength, long flength, long initoffset, int couples) 
             : base (path, slength, flength, initoffset, couples)
        {
            _id = id;
            c_thread = new Thread(ComputeData);
            c_thread.Name = "Cons" + _id;
            c_thread.Start();
        }

        private void ComputeData()
        {
            while  (!_islastblock)//(!_isstoped || (_queue.Count != 0))
            {
                ComputeBlock();
            }
        }
        
        private void ComputeBlock()
        {
            _hash.Initialize();
            byte[] c_datachunk = new byte[_SHA256block];
                        
            while (!_blockend || (_queue.Count != 0))
            {
                c_datachunk = ReadQueue();
                if (_queue.Count != 0)
                {
                    _hash.TransformBlock(c_datachunk, 0, c_datachunk.Length, c_datachunk, 0);
                }
                else
                {
                    _hash.TransformFinalBlock(c_datachunk, 0, c_datachunk.Length);
                }
                //Array.Clear(c_datachunk, 0, c_datachunk.Length);

                /*c_datachunk = ReadQueue();
                _hash.TransformBlock(c_datachunk, 0, c_datachunk.Length, c_datachunk, 0);*/
            }

            //_blockend = false;
            _waiting.Set();
            //_hash.TransformFinalBlock(c_datachunk, 0, c_datachunk.Length);
            c_datachunk = null;
            Board.Queue.SyncOut(_hash.Hash, _id);
        }
   

        
    }
}
