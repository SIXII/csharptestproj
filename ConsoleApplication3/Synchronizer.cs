﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.IO;
using System.Security.Cryptography;


namespace Signature
{
    public class Synchronizer

    {
        public long _sAmount;
        public long _sNumber;
        public int _cores;
        //private int _lockIN = 1; //Различать потоки будем по ID
        private int _lockOUT = 1;
        private readonly object _inlocker = new object();
        private readonly object _incontrolloker = new object();
        private readonly object _outcontrolloker = new object();
        //private Queue<long> _inqueue = new Queue<long>();
        
        

        public Synchronizer(long SignLength, long FileLenght, int Cores)
        {
            _sAmount = FileLenght/SignLength + ((FileLenght % SignLength > 0) ? 1 : 0);
            _cores = Cores;   
        }


        
        public void SyncOut(byte[] ToConvert, int ThreadId)
        {
            lock (_outcontrolloker)
            {
                SyncCore(ThreadId);
                _lockOUT = (_lockOUT < _cores) ? ++_lockOUT : 1;
                Interlocked.Increment(ref _sNumber);
                Console.WriteLine("Hash {0} says: {1} ({2})", _sNumber, BytesToStr(ToConvert),ThreadId);
                if (_sAmount == _sNumber)
                    Board.Signal.Set();
                Monitor.PulseAll(_outcontrolloker);
                
            }                     
        }

        private void SyncCore(int ThreadId) //Убедимся, что потоки получают и выводят
        {                                              //данные в правильном порядке
                lock (_outcontrolloker)
                {
                    while (ThreadId != _lockOUT)
                        Monitor.Wait(_outcontrolloker);
                    return;
                }
        }


        private string BytesToStr(byte[] Hash) //Простое конвертирование в строку перед выводом
        {
            StringBuilder Summ = new StringBuilder();

            for (int i = 0; i < Hash.Length; i++)
                Summ.AppendFormat("{0:X2}", Hash[i]);

            return Summ.ToString();
        }


    }
    
}
